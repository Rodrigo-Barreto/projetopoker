package br.com.repository;

import java.time.LocalDateTime;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.model.TorneioModel;
import br.com.repository.entity.TorneioEntity;
import br.com.repository.entity.UsuarioEntity;
import br.com.utils.Util;

public class TorneioRepository {

	@Inject
	TorneioEntity torneioEntity;

	EntityManager entityManager;

	/***
	 * MÉTODO RESPONSÁVEL POR SALVAR UM NOVO TORNEIO
	 * @param torneioModel
	 */
	public void SalvarNovoRegistro(TorneioModel torneioModel){

		entityManager =  Util.JpaEntityManager();

		torneioEntity = new TorneioEntity();
		torneioEntity.setTorneio(torneioModel.getTorneio());
		torneioEntity.setOrigemCadastro(torneioModel.getOrigemCadastro());

		UsuarioEntity usuarioEntity = entityManager.find(UsuarioEntity.class, torneioModel.getUsuarioModel().getCodigo()); 

		torneioEntity.setUsuarioEntity(usuarioEntity);

		entityManager.persist(torneioEntity);

	}
}
