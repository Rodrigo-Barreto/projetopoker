package br.com.model;


public class TorneioModel  {
	
	private Integer id;
	private String torneio;
	private String origemCadastro;
	private UsuarioModel usuarioModel;

	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTorneio() {
		return torneio;
	}
	public void setTorneio(String torneio) {
		this.torneio = torneio;
	}
	public UsuarioModel getUsuarioModel() {
		return usuarioModel;
	}
	public void setUsuarioModel(UsuarioModel usuarioModel) {
		this.usuarioModel = usuarioModel;
	}
	public String getOrigemCadastro() {
		return origemCadastro;
	}
	public void setOrigemCadastro(String origemCadastro) {
		this.origemCadastro = origemCadastro;
	}

}