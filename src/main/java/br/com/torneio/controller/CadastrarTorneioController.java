package br.com.torneio.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
 
import br.com.model.TorneioModel;
import br.com.repository.TorneioRepository;
import br.com.usuario.controller.UsuarioController;
import br.com.utils.Util;
 
@Named(value="cadastrarTorneioController")
@RequestScoped
public class CadastrarTorneioController {
 
	@Inject
	TorneioModel torneioModel;
 
	@Inject
	UsuarioController usuarioController;
 
	@Inject
	TorneioRepository torneioRepository;
 
 
	public TorneioModel getTorneioModel() {
		return torneioModel;
	}
 
	public void setTorneioModel(TorneioModel torneioModel) {
		this.torneioModel = torneioModel;
	}
 
	/**
	 *SALVA UM NOVO REGISTRO VIA INPUT 
	 */
	public void SalvarNovoTorneio(){
 
		torneioModel.setUsuarioModel(this.usuarioController.GetUsuarioSession());
 
		//INFORMANDO QUE O CADASTRO FOI VIA INPUT
		torneioModel.setOrigemCadastro("I");
 
		torneioRepository.SalvarNovoRegistro(this.torneioModel);
 
		this.torneioModel = null;
 
		Util.MensagemInfo("Registro cadastrado com sucesso");
		
 
	}
}
